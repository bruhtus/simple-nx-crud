import Fastify from 'fastify';

import { db } from '@simple-nx-crud/news/database';
import { newsRoutes } from '@simple-nx-crud/news/routes';

function app() {
  const fastify = Fastify();

  fastify.register(db);
  fastify.register(newsRoutes, { prefix: 'api/news' });

  return fastify;
}

export default app;
