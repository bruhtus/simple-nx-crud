import build from './app/app';

async function server() {
  const app = await build();
  const PORT = process.env.PORT || 8080;
  const host = '0.0.0.0';

  app.listen(PORT, host);
}

server();
