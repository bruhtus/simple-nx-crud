import * as mongoose from 'mongoose';
import build from '../app/app';

const app = build();

beforeAll(async () => {
  await app.ready();
});

afterAll(async () => {
  if (mongoose.connection.readyState === 1) {
    await mongoose.connection.dropDatabase()
    await mongoose.connection.close()
  }
});

async function getNewsId() {
  const newsId = await app.inject({
    method: 'GET',
    url: 'api/news'
  });

  return newsId.json()[0]._id;
}

describe('CRUD news exist', () => {
  const news = {
    title: 'typescript for web dev',
    content: 'types types types',
    topic: ['programming', 'web'],
    status: 'publish'
  };

  const newContent = 'why types?';

  test('can create news', async () => {
    const res = await app.inject({
      method: 'POST',
      url: 'api/news',
      payload: news
    });

    expect(res.statusCode).toBe(201);
    expect(res.json()).toMatchObject(news);
  });

  test('can find news by status', async () => {
    const res = await app.inject({
      method: 'GET',
      url: 'api/news',
      query: { status: news.status }
    });

    expect(res.statusCode).toBe(200);
    expect(res.json()).toEqual(
      expect.arrayContaining([
        expect.objectContaining(news)
      ])
    );
  });

  test('can find news by topic', async () => {
    const res = await app.inject({
      method: 'GET',
      url: 'api/news',
      query: { topic: news.topic[0] }
    });

    expect(res.statusCode).toBe(200);
    expect(res.json()).toEqual(
      expect.arrayContaining([
        expect.objectContaining(news)
      ])
    );
  });

  test('can find news by multiple topics', async () => {
    const res = await app.inject({
      method: 'GET',
      url: 'api/news',
      query: { topics: 'programming,web' }
    });

    expect(res.statusCode).toBe(200);
    expect(res.json()).toEqual(
      expect.arrayContaining([
        expect.objectContaining(news)
      ])
    );
  });

  test('can find news by id', async () => {
    const id = await getNewsId();
    const res = await app.inject({
      method: 'GET',
      url: `api/news/${id}`
    });

    expect(res.statusCode).toBe(200);
    expect(res.json()).toMatchObject(news);
  });

  test('can update news', async () => {
    const id = await getNewsId();
    const res = await app.inject({
      method: 'PUT',
      url: `api/news/${id}`,
      payload: { content: newContent }
    });

    expect(res.statusCode).toBe(201);
    expect(res.json()).toMatchObject({
      ...news,
      content: newContent
    });
  });

  test('can delete news', async () => {
    const id = await getNewsId();
    const res = await app.inject({
      method: 'DELETE',
      url: `api/news/${id}`
    });

    expect(res.statusCode).toBe(200);
  });

  // make sure the deleted news really got deleted.
  test('can not retrieve deleted news', async () => {
    const res = await app.inject({
      method: 'GET',
      url: 'api/news'
    });

    const testNews = res.json().filter(n => n.title === news.title);

    expect(testNews.length).toBe(0);
  });

  test('can denied post request without title', async () => {
    const { title, ...newsWithoutTitle } = news;
    const res = await app.inject({
      method: 'POST',
      url: 'api/news',
      payload: newsWithoutTitle
    });

    expect(res.statusCode).toBe(400);
  });
});
