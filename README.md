# Simple Nx Crud

This project was generated using [Nx](https://nx.dev).

<p style="text-align: center;"><img src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-logo.png" width="450"></p>

🔎 **Smart, Fast and Extensible Build System**

Other than Nx, this project also use fastify and mongodb to make a simple REST
API.

## Step to Generate Nx Node Workspace

1. `npx create-nx-workspace@latest simple-nx-crud --package-manager=yarn`
2. `cd simple-nx-crud`
3. `yarn add @nrwl/node --dev`
4. `yarn nx g @nrwl/node:app news`

## Step to Generate Nx Node Library

1. `yarn nx g @nrwl/node:lib routes --directory=news`
2. `yarn nx g @nrwl/node:lib database --directory=news`

> Use flag `--dry-run` to check the generated library before actually write to
> disk.

## Simple Guide to Run This Project locally

Here's the general step to run this project locally:
- Clone this project using your favorite git GUI app or using this command:
```sh
git clone https://gitlab.com/bruhtus/simple-nx-crud.git
```

- Assuming you haven't rename the directory or didn't put another argument
after the `git clone` command, and also you're still in the same directory
where you clone this project, change directory to project directory using
this command:
```sh
cd simple-nx-crud
```

- Make sure you're in the right directory by using this command:
```sh
pwd
```
and the result is something like this:
```sh
# like the name implied, optional directory is optional, if clone this project
# in home directory, then you can ignore optional directory part.
/home/username/<optional-directory>/simple-nx-crud
```

- Install the dependency with this command:
```sh
yarn install
```

- Make sure that docker is installed. If you're using unix-like operating
system and using POSIX compliant shell, you can use this command:
```sh
command -v docker
```
if the output is empty, you can take a look at
[docker documentation](https://docs.docker.com/get-docker/) to find out how to install docker.

- After docker installed, run the mongodb instance with docker using this
command:
```sh
docker run --name mongodb-local -dit -p 27017:27017 --rm mongo:4.4.1
```

- After that initialize environment variable `MONGO_URI` with this command:
```sh
export MONGO_URI='mongodb://localhost:27017/news'
```

- To run the server, use this command:
```sh
yarn build && yarn start
```

- To run the test, use this command:
```sh
yarn test
```

This project is deployed on
[https://simple-nx-crud.herokuapp.com/api/news](https://simple-nx-crud.herokuapp.com/api/news)

## References

- [Frontend master: introduction to nodejs, v2](https://frontendmasters.com/courses/node-js-v2/).
- [Frontend master: api design in nodejs, v3](https://frontendmasters.com/courses/api-design-nodejs-v3/).
- [Frontend master: complete intro to database](https://frontendmasters.com/courses/databases/).
- [Frontend master: javascript from fundamentals to functionals, v2](https://frontendmasters.com/courses/js-fundamentals-functional-v2/).
- [Udemy course: master fullstack - react, fastify node.js, postgres, tdd](https://www.udemy.com/course/fullstack-project-react-fastify-nodejs-postgresql-tdd/).
- [Example of findOneAndUpdate() in mongoose](https://mongoosejs.com/docs/tutorials/findoneandupdate.html).
- [Jest expect documentation](https://jestjs.io/docs/expect).
- [Use yarn as default package manager in Nx](https://github.com/nrwl/nx/issues/4513#issuecomment-759708345).
- [Import supertest](https://tutorialedge.net/typescript/testing-typescript-api-with-jest/).
- [Built-in body parser in express](https://dev.to/taylorbeeston/you-probably-don-t-need-body-parser-in-your-express-apps-3nio).
- [Mongoose with typescript](https://mongoosejs.com/docs/typescript.html).
- [Unit testing node.js + mongoose using jest](https://javascript.plainenglish.io/unit-testing-node-js-mongoose-using-jest-106a39b8393d).
- [Generate new nx node project](https://nx.dev/node/overview).
- [Print out coverage test report](https://github.com/nrwl/nx/issues/1337#issuecomment-492391792).
- [Delete all documents in a collection on mongodb](https://docs.mongodb.com/manual/reference/method/db.collection.deleteMany/).
- [Mongoose server selection timeout](https://mongoosejs.com/docs/connections.html#server-selection).
- [Jest matching object in array medium post](https://medium.com/@andrei.pfeiffer/jest-matching-objects-in-array-50fe2f4d6b98).
- [Example how to shutting down the connection with fastify](https://github.com/fastify/fastify/issues/1367#issuecomment-453206962).
- [Fastify request documentation](https://github.com/fastify/fastify/blob/main/docs/Reference/Request.md).
- [Setup github CI blog post](https://dev.to/6thcode/how-to-set-up-a-cicd-environment-on-gitlab-using-nodejs-jh3).
- [Keyword reference for .gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/).
- [Setup env for gitlab CI](https://stackoverflow.com/questions/42966645/setting-up-postgresql-in-gitlab-ci).
- [Setup mongodb cluster with heroku](https://www.mongodb.com/developer/how-to/use-atlas-on-heroku/).
- [Deploy using gitlab ci/cd to heroku medium post](https://medium.com/swlh/how-do-i-deploy-my-code-to-heroku-using-gitlab-ci-cd-6a232b6be2e4).
- [Stackoverflow answer: passing the host to heroku](https://stackoverflow.com/a/47420642).
- [Nx generate lib](https://nx.dev/node/library).
- [Nx example github repo](https://github.com/nrwl/nx-examples).
