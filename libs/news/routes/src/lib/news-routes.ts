import { newsModel } from '@simple-nx-crud/news/database';

function crudControllers(model) {
  async function createOne(req, res) {
    const doc = await model.create(req.body);
    return res.status(201).send(req.body);
  }

  async function getAll(req, res) {
    const { query } = req;

    if (query.topics) {
      const docs = await model.find({
        ...query,
        topic: query.topics.split(',')
      });
      return res.status(200).send(docs);
    }

    const docs = await model.find(req.query);
    return res.status(200).send(docs);
  }

  async function getOneById(req, res) {
    const doc = await model.findById({
      _id: req.params.id
    });
    return res.status(200).send(doc);
  }

  async function updateOneById(req, res) {
    const doc = await model.findByIdAndUpdate(
      { _id: req.params.id },
      req.body,
      { new: true }
    )

    return res.status(201).send(doc);
  }

  async function removeOneById(req, res) {
    const deleted = await model.findByIdAndRemove({
      _id: req.params.id
    });
    return res.status(200).send(deleted);
  }

  return { createOne, getAll, getOneById, updateOneById, removeOneById };
}

export function newsRoutes(fastify, config, next) {
  const {
    createOne,
    getAll,
    getOneById,
    updateOneById,
    removeOneById
  } = crudControllers(newsModel);

  const postRequestSchema = {
    type: 'object',
    required: ['title'],
    properties: {
      title: {
        type: 'string'
      },
      content: {
        type: 'string'
      },
      topic: {
        type: 'array'
      },
      status: {
        type: 'string'
      },
    }
  };

  const postOne = fastify.post(
    '/',
    { schema: { body: postRequestSchema } },
    createOne
  );
  const getEverything = fastify.get('/', getAll);

  const getOne = fastify.get('/:id', getOneById);
  const putOne = fastify.put('/:id', updateOneById);
  const deleteOne = fastify.delete('/:id', removeOneById);

  next();
}
