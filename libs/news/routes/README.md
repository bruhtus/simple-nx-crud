# news-routes

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test news-routes` to execute the unit tests via [Jest](https://jestjs.io).
