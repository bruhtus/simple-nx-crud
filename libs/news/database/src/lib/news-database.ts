import fp from 'fastify-plugin';
import * as mongoose from 'mongoose';

async function newsDatabase(fastify, config, next) {
  const connect = await mongoose.connect(process.env.MONGO_URI, {
    serverSelectionTimeoutMS: 1000
  });

  fastify.decorate('dbConnect', connect);

  next();
}

export const db = fp(newsDatabase);
