import * as mongoose from 'mongoose';

export interface News {
  title: string;
  content: string;
  topic: string[];
  status: string;
}

const newsSchema = new mongoose.Schema<News>({
  title: {
    type: String,
    required: true
  },
  content: String,
  topic: [String],
  status: String
});

export const newsModel = mongoose.model<News>('news', newsSchema);
